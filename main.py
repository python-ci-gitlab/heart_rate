from datetime import datetime, timedelta

from sqlalchemy import func

from init_db import User, HeartRate, session, populate_data


def query_users(
        _min_age: int,
        _gender: str,
        _min_avg_heart_rate: float,
        _date_from: datetime,
        _date_to: datetime
) -> list[User]:
    """
    Query for users older than `min_age`, of a specific `gender`,
    having an average heart rate higher than `min_avg_heart_rate`
    between `date_from` and `date_to`.
    """
    _users = session.query(User).join(HeartRate).filter(
        User.age > _min_age,
        User.gender == _gender,
        HeartRate.timestamp.between(_date_from, _date_to)
    ).group_by(User.id).having(
        func.avg(HeartRate.heart_rate) > _min_avg_heart_rate
    ).all()
    return _users


def query_for_user(
        _user_id: int,
        _date_from: datetime,
        _date_to: datetime
) -> list[float]:
    """
    Query for the top 10 highest average heart rates
    for hourly intervals within the specified period for a user.
    """
    avg_heart_rate = session.query(
        func.avg(HeartRate.heart_rate).label('average_heart_rate')
    ).filter(
        HeartRate.user_id == _user_id,
        HeartRate.timestamp.between(_date_from, _date_to)
    ).group_by(
        func.extract('hour', HeartRate.timestamp)
    ).order_by(
        func.avg(HeartRate.heart_rate).desc()
    ).limit(10).all()
    return [result.average_heart_rate for result in avg_heart_rate]


if __name__ == "__main__":
    # Populate the database with sample data
    populate_data()

    # Define query parameters
    min_age = 30
    gender = 'male'
    min_avg_heart_rate = 70.0
    date_from = datetime.now() - timedelta(days=30)
    date_to = datetime.now()

    # Query users
    users = query_users(min_age, gender, min_avg_heart_rate, date_from, date_to)
    print("Users matching criteria:")
    for user in users:
        print(f'User ID: {user.id}, Name: {user.name}, Avg Heart Rate: {min_avg_heart_rate}')

    # Query for a specific user
    if users:
        user_id = users[0].id  # Example user ID
        top_heart_rates = query_for_user(user_id, date_from, date_to)
        print(f'\n‹Top 10 hourly average heart rates for User ID {user_id}: {top_heart_rates}')
