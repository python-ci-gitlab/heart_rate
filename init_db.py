import random

from sqlalchemy import create_engine, Column, Integer, String, Float, DateTime, ForeignKey
from sqlalchemy.orm import declarative_base, sessionmaker, relationship
from datetime import datetime, timedelta

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    gender = Column(String)
    age = Column(Integer)
    heart_rates = relationship("HeartRate", back_populates="user")


class HeartRate(Base):
    __tablename__ = 'heart_rates'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    timestamp = Column(DateTime)
    heart_rate = Column(Float)
    user = relationship("User", back_populates="heart_rates")


engine = create_engine('postgresql://postgres:postgres@localhost:5432/db')
Session = sessionmaker(bind=engine)
session = Session()
Base.metadata.create_all(engine)


def populate_data():
    # Create sample users
    users = [
        User(name='Alice', gender='female', age=28),
        User(name='Bob', gender='male', age=32),
        User(name='Charlie', gender='male', age=45),
    ]
    session.add_all(users)
    session.commit()

    # Generate heart rate data for the users
    for user in users:
        for _ in range(15):  # 100 random heart rate records
            timestamp = datetime.now() - timedelta(days=random.randint(0, 30), hours=random.randint(0, 23))
            heart_rate = round(random.uniform(60.0, 100.0), 1)
            heart_rate_record = HeartRate(user_id=user.id, timestamp=timestamp, heart_rate=heart_rate)
            session.add(heart_rate_record)

    session.commit()
